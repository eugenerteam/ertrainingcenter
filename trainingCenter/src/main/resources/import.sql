insert into SITE_USERS (username, password, enabled, site_role, email) values('user-a@tc.edu','123','true','User','user-a@tc.edu');
insert into SITE_USERS (username, password, enabled, site_role, email) values('user-b@tc.edu','123','true','User','user-b@tc.edu');
insert into SITE_USERS (username, password, enabled, site_role, email) values('user-c@tc.edu','123','true','User','user-c@tc.edu');
insert into SITE_USERS (username, password, enabled, site_role, email) values('lecturer-a@tc.edu','123','true','Lecturer','rewgen@tut.by');
insert into SITE_USERS (username, password, enabled, site_role, email) values('lecturer-b@tc.edu','123','true','Lecturer','lecturer-b@tc.edu');
insert into SITE_USERS (username, password, enabled, site_role, email, full_name) values('dm@tc.edu','123','true','Department manager','rewgen1@gmail.com','Alex Ivanov');
insert into SITE_USERS (username, password, enabled, site_role, email, full_name) values('km@tc.edu','123','true','Knowlege manager','dmitry.lituev@servolux.by','Simon Sharapov');
insert into USER_ROLES (username, role) values('user-a@tc.edu','ROLE_USER');
insert into USER_ROLES (username, role) values('user-b@tc.edu','ROLE_USER');
insert into USER_ROLES (username, role) values('user-c@tc.edu','ROLE_USER');
insert into USER_ROLES (username, role) values('lecturer-a@tc.edu','ROLE_USER');
insert into USER_ROLES (username, role) values('lecturer-b@tc.edu','ROLE_USER');
insert into USER_ROLES (username, role) values('dm@tc.edu','ROLE_USER');
insert into USER_ROLES (username, role) values('km@tc.edu','ROLE_USER');
insert into CATEGORIES (name) values ('Project management');
insert into CATEGORIES (name) values ('Development');

insert into COURSE_STATES (state_name) values ('Draft');
insert into COURSE_STATES (state_name) values ('Proposal');
insert into COURSE_STATES (state_name) values ('Rejected');
insert into COURSE_STATES (state_name) values ('New');
insert into COURSE_STATES (state_name) values ('Open');
insert into COURSE_STATES (state_name) values ('Ready');
insert into COURSE_STATES (state_name) values ('In Progress');
insert into COURSE_STATES (state_name) values ('Finished');

insert into COURSES (name, description, links, owner, category_id, state_name, min_subs_qty, min_atts_qty) values('Course1','Descr1','link1','lecturer-a@tc.edu',1,'Draft',2,2);
insert into COURSES (name, description, links, owner, category_id, state_name, min_subs_qty, min_atts_qty) values('Course2','Descr2','link2','lecturer-a@tc.edu',1,'Proposal',2,2);
insert into COURSES (name, description, links, owner, category_id, state_name, min_subs_qty, min_atts_qty) values('Course3','Descr3','link3','lecturer-a@tc.edu',1,'Rejected',2,2);
insert into COURSES (name, description, links, owner, category_id, state_name, min_subs_qty, min_atts_qty) values('Course4','Deskr4','link4','lecturer-a@tc.edu',1,'New',2,2);
insert into COURSES (name, description, links, owner, category_id, state_name, min_subs_qty, min_atts_qty) values('Course5','Descr5','link5','lecturer-a@tc.edu',1,'Open',2,2);
insert into COURSES (name, description, links, owner, category_id, state_name, min_subs_qty, min_atts_qty) values('Course6','Descr6','link6','lecturer-a@tc.edu',2,'Ready',2,2);
insert into COURSES (name, description, links, owner, category_id, state_name, min_subs_qty, min_atts_qty) values('Course7','Descr7','link7','lecturer-a@tc.edu',2,'In Progress',2,2);
insert into COURSES (name, description, links, owner, category_id, state_name, min_subs_qty, min_atts_qty) values('Course8','Descr8','link8','lecturer-a@tc.edu',2,'Finished',2,2);

insert into SUBSCRIBES (username, course_id, status, grade) values ('lecturer-a@tc.edu',1,'owner',0);
insert into SUBSCRIBES (username, course_id, status, grade) values ('lecturer-a@tc.edu',2,'owner',0);
insert into SUBSCRIBES (username, course_id, status, grade) values ('lecturer-a@tc.edu',3,'owner',0);
insert into SUBSCRIBES (username, course_id, status, grade) values ('lecturer-a@tc.edu',4,'owner',0);
insert into SUBSCRIBES (username, course_id, status, grade) values ('lecturer-a@tc.edu',5,'owner',0);
insert into SUBSCRIBES (username, course_id, status, grade) values ('lecturer-a@tc.edu',6,'owner',0);
insert into SUBSCRIBES (username, course_id, status, grade) values ('lecturer-a@tc.edu',7,'owner',0);
insert into SUBSCRIBES (username, course_id, status, grade) values ('lecturer-a@tc.edu',8,'owner',0);

insert into SUBSCRIBES (username, course_id, status, grade) values ('user-a@tc.edu',4,'subscriber',0);
insert into SUBSCRIBES (username, course_id, status, grade) values ('user-a@tc.edu',5,'attendee',0);
insert into SUBSCRIBES (username, course_id, status, grade) values ('user-a@tc.edu',6,'attendee',0);
insert into SUBSCRIBES (username, course_id, status, grade) values ('user-a@tc.edu',7,'subscriber',0);
insert into SUBSCRIBES (username, course_id, status, grade) values ('user-a@tc.edu',8,'graduated',3);

insert into SUBSCRIBES (username, course_id, status, grade) values ('user-b@tc.edu',5,'subscriber',0);
insert into SUBSCRIBES (username, course_id, status, grade) values ('user-b@tc.edu',6,'attendee',0);
insert into SUBSCRIBES (username, course_id, status, grade) values ('user-b@tc.edu',7,'attendee',0);
insert into SUBSCRIBES (username, course_id, status, grade) values ('user-b@tc.edu',8,'attendee',0);

insert into COURSES_APP (username, course_id, status, type, reason) values ('km@tc.edu',2,0,1,'');
insert into COURSES_APP (username, course_id, status, type, reason) values ('dm@tc.edu',2,0,2,'');

insert into COURSES_APP (username, course_id, status, type, reason) values ('km@tc.edu',3,-1,1,'some reasons');
insert into COURSES_APP (username, course_id, status, type, reason) values ('dm@tc.edu',3,-1,2,'another reasons');

insert into COURSES_APP (username, course_id, status, type, reason) values ('km@tc.edu',4,1,1,'good course');
insert into COURSES_APP (username, course_id, status, type, reason) values ('dm@tc.edu',4,1,2,'agree');

insert into EVENTS (name,recipient,copy_recipient,template,link_part,subject) values ('propose','role:%manager','type:owner','annonce','approve','Course Announcement');
insert into EVENTS (name,recipient,copy_recipient,template,link_part,subject) values ('approveupd','type:owner','role:%manager','approveupd','','Course Approval Update');
insert into EVENTS (name,recipient,copy_recipient,template,link_part,subject) values ('added','role:User','role:%manager, type:owner','added','subscribe','New Course Added');
insert into EVENTS (name,recipient,copy_recipient,template,link_part,subject) values ('rejected','type:owner','role:%manager','rejected','','Course was rejected');
insert into EVENTS (name,recipient,copy_recipient,template,link_part,subject) values ('deleted','type:owner','role:%manager','deleted','','Course was deleted');
insert into EVENTS (name,recipient,copy_recipient,template,link_part,subject) values ('opened','type:subscriber','type:owner','opened','attend','Course was opened to attend');
insert into EVENTS (name,recipient,copy_recipient,template,link_part,subject) values ('ready','type:subscriber, type:attendee','type:owner','ready','','Course ready to start and wait for lecturer decision');
insert into EVENTS (name,recipient,copy_recipient,template,link_part,subject) values ('started','type:attendee','type:owner','started','','Course has been started');
insert into EVENTS (name,recipient,copy_recipient,template,link_part,subject) values ('finished','type:attendee','type:owner','finished','evaluate','Course has been finished');
insert into EVENTS (name,recipient,copy_recipient,template,link_part,subject) values ('notify','type:attendee','type:owner','notify','evaluate','Please, do not remember evaluate the course!');


