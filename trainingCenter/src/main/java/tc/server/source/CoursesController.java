package tc.server.source;

import tc.server.source.dao.*;
import tc.server.source.service.ModelsBuilder;
import tc.server.source.service.CourseService;
import tc.server.source.service.UserService;
import tc.server.source.tables.Course;
import tc.server.source.tables.CourseState;
import tc.server.source.tables.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import tc.server.source.models.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by evgeniy.ranchenko on 24.11.2016.
 */
@Controller
@RequestMapping("/courses")
@SessionAttributes({"errorMessage","transitError"})
public class CoursesController {

    @Autowired
    private ModelsBuilder modelsBuilder;

    @Autowired
    private CourseService courseService;

    @Autowired
    private UserService userService;

    @RequestMapping
    public String getStartPage(ModelMap model, Principal principal, CategoryModel categoryModel, HttpSession session){
        model.remove("errorMessage");
        session.removeAttribute("errorMessage");

        model.addAttribute("categoryList",courseService.getCategoryList());
        if (categoryModel.getId()==0){
            model.addAttribute("categoryModel",categoryModel);
        }

        model.addAttribute("coursesList", modelsBuilder.getCoursesWithStatusList(principal.getName(),categoryModel.getId()));
        boolean isLecturer = userService.isLecturer(principal.getName());
        model.addAttribute("isLecturer",isLecturer);
        return "courses";
    }

    @RequestMapping("/{details}")
    public String getDetails(@PathVariable("details") String details, ModelMap model, Principal principal){

        CourseWithStatus courseWithStatus = modelsBuilder.buildCourseWithStatus(details, principal.getName());
        if (courseWithStatus==null || !courseWithStatus.isVisible()) return "badid";

        CourseModel courseModel = modelsBuilder.buildCourseModelByCWS(courseWithStatus, null);

        model.addAttribute("course",courseModel);
        model.addAttribute("subscrQty",courseWithStatus.getSubscribersCount());
        model.addAttribute("attQty",courseWithStatus.getAttendeeCount());
        model.addAttribute("grade",courseWithStatus.getGrade());
        return "detail";
    }

    @RequestMapping("/{details}/participants")
    public String getParticipants(@PathVariable("details") String details, ModelMap model, Principal principal){

        CourseWithStatus courseWithStatus = modelsBuilder.buildCourseWithStatus(details, principal.getName());
        if (courseWithStatus==null || !courseWithStatus.isVisible()) return "badid";

        model.addAttribute("course",modelsBuilder.getCourseWithParticipants(details));
        model.addAttribute("isSubscriber",courseWithStatus.isSubscriber());
        model.addAttribute("isAttendee",courseWithStatus.isAttandee());
        model.addAttribute("isOwner",courseWithStatus.isOwner());
        return "participants";
    }

    @RequestMapping("/create")
    public String getCreatePage(@Valid CourseModel courseModel, BindingResult bindingResult, ModelMap model, Principal principal){
        String result = "create";

        boolean isLecturer = userService.isLecturer(principal.getName());
        if (!isLecturer) return "forbidden";

        if (courseModel.getName() != null){ // first time name==null
            if (!bindingResult.hasErrors()){
                courseService.addCourse(courseModel,principal.getName());

                model.remove("errorMessage");
                result = "redirect:/courses";
                return result;
            }else{
                model.addAttribute("errorMessage", getBindingErrorList(bindingResult));
            }

        }else{ //new course
            model.remove("errorMessage");
            courseModel= modelsBuilder.buildModelNewCourse(courseModel);
            model.addAttribute("courseModel",courseModel);
        }
        model.addAttribute("actionType","create");
        model.addAttribute("actionText","Create");
        model.addAttribute("categoryList",courseService.getCategoryList());
        return result;
    }


    @RequestMapping("/{details}/update")
    public String getDetailsUpdate(@PathVariable("details") String details, @Valid CourseModel courseModel, BindingResult bindingResult, Principal principal, ModelMap model){
        String result="update";

        CourseWithStatus courseWithStatus = modelsBuilder.buildCourseWithStatus(details, principal.getName());

        boolean isLecturer = courseWithStatus.isLecturer();
        if (!isLecturer) return "forbidden";
        if (courseWithStatus.getCourse()==null) return "badid";

        //if you owner but proposal - you not owner he-he
        boolean isOwner = courseWithStatus.isOwner() && !courseWithStatus.isProposal();
        if (!isOwner) return "forbidden";

        if(courseModel.getName()!=null){ //not first time
            if (!bindingResult.hasErrors()){ //update OK
                if (courseModel.getReview()!=null) courseModel.setCourseState(CourseState.PROPOSAL);
                    courseService.updateCourse(details, courseModel);

                    model.remove("errorMessage");
                    result = "redirect:/courses";
                    return result;
            }else{
                model.addAttribute("errorMessage", getBindingErrorList(bindingResult));
            }
        }else{ //first time
            model.remove("errorMessage");
            courseModel = modelsBuilder.buildCourseModel(details, courseModel);
            model.addAttribute("courseModel",courseModel);
        }

        boolean reviewAvailable = courseWithStatus.isEditable();
        model.addAttribute("reviewAvailable",reviewAvailable);

        model.addAttribute("actionType",details+"/update");
        model.addAttribute("actionText","Update");
        model.addAttribute("categoryList",courseService.getCategoryList());

        return result;
    }

    @RequestMapping("/{details}/subscribe")
    public String getSubscribe(@PathVariable("details") String details, ModelMap model, Principal principal){
        CourseWithStatus courseWithStatus = modelsBuilder.buildCourseWithStatus(details, principal.getName());

        if (courseWithStatus==null
                || !courseWithStatus.isVisible()
                || !courseWithStatus.isAvailableForSubscribe()) return "badid";

        model.addAttribute("course", modelsBuilder.buildSubscribeModelByCWS(courseWithStatus,null));
        return "subscribe";
    }

    @RequestMapping("/subscribe")
    public String doSubscribe(SubscribeModel subscribeModel, Principal principal){
        courseService.addSubscription(subscribeModel.getCourseId(),principal.getName());
        return "redirect:/courses";
    }

    @RequestMapping("/{details}/attend")
    public String getAttend(@PathVariable("details") String details, ModelMap model, Principal principal){

        CourseWithStatus courseWithStatus = modelsBuilder.buildCourseWithStatus(details, principal.getName());
        if (courseWithStatus==null || !courseWithStatus.isVisible()) return "badid";

         if (courseWithStatus.isAvailableForAttend()) model.addAttribute("course", modelsBuilder.buildSubscribeModelByCWS(courseWithStatus,null));
            else return "forbidden";

        return "attend";
    }

    @RequestMapping("/attend")
    public String doAttend(SubscribeModel subscribeModel, Principal principal){
        courseService.addAttend(subscribeModel.getCourseId(),principal.getName());
        return "redirect:/courses";
    }

    @RequestMapping("/{details}/evaluate")
    public String getEvaluate(@PathVariable("details") String details, ModelMap model, EvaluationModel evaluationModel, Principal principal, HttpSession session){
        CourseWithStatus courseWithStatus = modelsBuilder.buildCourseWithStatus(details, principal.getName());

        if (model.containsAttribute("transitError")){ //but remain "errorMesaage'
            model.remove("transitError");
            session.removeAttribute("transitError");
        }else if (model.containsAttribute("errorMessage")){ //in other cases clear errorMesage
            model.remove("errorMessage");
            session.removeAttribute("errorMessage");
        }

        if (courseWithStatus==null || !courseWithStatus.isVisible()) return "badid";

        evaluationModel = (EvaluationModel) modelsBuilder.buildSubscribeModelByCWS(courseWithStatus,evaluationModel);
        if (courseWithStatus.isAvailableForEvaluate()) model.addAttribute("evaluationModel",evaluationModel); //only attendee may evaluate
           else return "forbidden";

        return "evaluate";
    }

    @RequestMapping("/evaluate")
    public String doEvaluate(@Valid EvaluationModel evaluationModel, BindingResult bindingResult, ModelMap model, HttpSession session, Principal principal){
        String result = "redirect:/courses";
        model.remove("transitError");
        session.removeAttribute("transitError");

        if (bindingResult.hasErrors()){
            model.addAttribute("errorMessage", getBindingErrorList(bindingResult));
            result+="/"+evaluationModel.getCourseId()+"/evaluate";
            model.addAttribute("transitError","yes");
        }else{
            model.remove("errorMessage");
            session.removeAttribute("errorMessage");
            courseService.addGrade(evaluationModel.getCourseId(),principal.getName(),evaluationModel.getGrade());
        }
        return result;
    }

    @RequestMapping("/{details}/approve")
    public String getApprove(@PathVariable("details") String details, ModelMap model, Principal principal, ApproveModel approveModel){

        CourseWithStatus courseWithStatus = modelsBuilder.buildCourseWithStatus(details, principal.getName());

        if (courseWithStatus==null || !courseWithStatus.isVisible()) return "badid";
        if (!courseWithStatus.isDM()&&!courseWithStatus.isKM()) return "forbidden";

        approveModel = modelsBuilder.buildApproveModelByCWS(courseWithStatus,approveModel);

        model.addAttribute("approveModel",approveModel);
        model.addAttribute("course", modelsBuilder.buildCourseModelByCWS(courseWithStatus,null));

        return "approve";
    }


    @RequestMapping("/approve")
    public String proceedApprove(ApproveModel approveModel, ModelMap model, Principal principal, HttpSession session){
        session.removeAttribute("errorMessage");
        if (approveModel!=null){
            List<String> errors = validateApproveModel(approveModel);
            if (errors!=null){
                model.addAttribute("errorMessage", errors);
                return "redirect:/courses/"+approveModel.getCourseId()+"/approve";
            }

            courseService.updateApprove(approveModel, principal.getName());
        }
        return "redirect:/courses";
    }

    @RequestMapping("/{details}/delete")
    public String getDelete(@PathVariable("details") String details, ModelMap model, Principal principal, HttpSession session){
        CourseWithStatus courseWithStatus = modelsBuilder.buildCourseWithStatus(details, principal.getName());

        if (courseWithStatus==null || !courseWithStatus.isOwner()) return "badid";
        if (!courseWithStatus.isDraft() && !courseWithStatus.isRejected()) return "forbidden";

        model.addAttribute("course", modelsBuilder.buildCourseModelByCWS(courseWithStatus,null));
        model.addAttribute("courseId",details);
        return "delete";
    }

    @RequestMapping("/delete")
    public String proceedDelete(@Valid SubscribeModel subscribeModel, BindingResult bindingResult, ModelMap model, HttpSession session, Principal principal){
        courseService.deleteCourse(subscribeModel.getCourseId());
        return "redirect:/courses";
    }

    @RequestMapping("/{details}/start")
    public String getStart(@PathVariable("details") String details, ModelMap model, Principal principal){
        CourseWithStatus courseWithStatus = modelsBuilder.buildCourseWithStatus(details, principal.getName());

        if (courseWithStatus==null || !courseWithStatus.isOwner()) return "badid";
        else if (!courseWithStatus.isReady())return "forbidden";

        model.addAttribute("course", modelsBuilder.buildCourseModelByCWS(courseWithStatus, null));
        model.addAttribute("courseId",details);
        return "start";
    }

    @RequestMapping("/start")
    public String proceedStart(@Valid SubscribeModel subscribeModel, BindingResult bindingResult, ModelMap model, HttpSession session, Principal principal){
        String result = "redirect:/courses";

        CourseModel courseModel= new CourseModel();
        courseModel.setCourseState(CourseState.IN_PROGRESS);
        courseService.updateCourse(subscribeModel.getCourseId(),courseModel);

        return result;
    }

    @RequestMapping("/{details}/finish")
    public String getFinish(@PathVariable("details") String details, ModelMap model, Principal principal){
        CourseWithStatus courseWithStatus = modelsBuilder.buildCourseWithStatus(details, principal.getName());

        if (courseWithStatus==null || !courseWithStatus.isOwner()) return "badid";
        else if (!courseWithStatus.isInProgress()) return "forbidden";

        model.addAttribute("course", modelsBuilder.buildCourseModelByCWS(courseWithStatus,null));
        model.addAttribute("courseId",details);
        return "finish";
    }

    @RequestMapping("/finish")
    public String proceedFinish(@Valid SubscribeModel subscribeModel, BindingResult bindingResult, ModelMap model){
        String result = "redirect:/courses";

        CourseModel courseModel= new CourseModel();
        courseModel.setCourseState(CourseState.FINISHED);
        courseService.updateCourse(subscribeModel.getCourseId(),courseModel);

        return result;
    }

    @RequestMapping("/{details}/notify")
    public String getNotify(@PathVariable("details") String details, ModelMap model, Principal principal){
        CourseWithStatus courseWithStatus = modelsBuilder.buildCourseWithStatus(details, principal.getName());

        if (courseWithStatus==null || !courseWithStatus.isOwner()) return "badid";
        else if (!courseWithStatus.isFinished()) return "forbidden";

        model.addAttribute("course", modelsBuilder.buildCourseModelByCWS(courseWithStatus,null));
        model.addAttribute("courseId",details);
        return "notify";
    }

    @RequestMapping("/notify")
    public String proceedNotify(@Valid SubscribeModel subscribeModel, BindingResult bindingResult, ModelMap model, HttpSession session, Principal principal){
        String result = "redirect:/courses";
        courseService.sendNotify(subscribeModel.getCourseId());
        return result;
    }

    private List<String> validateApproveModel(ApproveModel approveModel){
        List<String> errors =null;
        if ((approveModel.isKM() && approveModel.getKMStatus()==0)
                ||(approveModel.isDM() && approveModel.getDMStatus()==0) ){
            errors = new ArrayList<>();
            errors.add("Reason is required");
        }
        return errors;
    }

    private List<String> getBindingErrorList(BindingResult bindingResult){
        List<String> error= new ArrayList<>();
        List<ObjectError> list= bindingResult.getAllErrors();
        for (ObjectError e : list){
            error.add(e.getDefaultMessage());
        }
        return error;
    }

}
