package tc.server.source.tables;

import javax.persistence.*;

/**
 * Created by evgeniy.ranchenko on 13.12.2016.
 */
@Entity
@Table(name = "courses_app")
public class CourseApprovement {
    public static final long NOSTATUS = 0;
    public static final long APPROVED = 1;
    public static final long REJECTED = -1;
    public static final long TYPE_KM = 1;
    public static final long TYPE_DM = 2;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "app_id")
    private long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "username")
    private User user;//who approve or reject

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "course_id")
    private Course course;

    @Column(name = "status")
    private long status;

    @Column(name = "type", nullable = false)
    private long type;

    @Column(name = "reason")
    private String reason;

    public CourseApprovement() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public long getStatus() {
        return status;
    }

    public void setStatus(long status) {
        this.status = status;
    }

    public long getType() {
        return type;
    }

    public void setType(long type) {
        this.type = type;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
