package tc.server.source.tables;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by User on 11.12.2016.
 */
@Entity
@Table(name="course_states")
public class CourseState {
    public static final String DRAFT = "Draft";
    public static final String PROPOSAL = "Proposal";
    public static final String REJECTED = "Rejected";
    public static final String NEW = "New";
    public static final String OPEN = "Open";
    public static final String READY = "Ready";
    public static final String IN_PROGRESS = "In Progress";
    public static final String FINISHED = "Finished";

    @Id
    @Column(name = "state_name", unique = true, nullable = false, length = 15)
    private String name;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "courseState")
    Set<Course> courses = new HashSet<>();

    public CourseState() {
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Course> getCourses() {
        return courses;
    }

    public void setCourses(Set<Course> courses) {
        this.courses = courses;
    }
}
