package tc.server.source.tables;

import javax.persistence.*;

/**
 * Created by evgeniy.ranchenko on 08.12.2016.
 */
@Entity
@Table(name = "subscribes")
public class Subscribe {
    public static final String SUBSCRIBER = "subscriber";
    public static final String ATTENDEE = "attendee";
    public static final String GRADUATED = "graduated";
    public static final String OWNER = "owner";


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "subscribes_id")
    private long id;

    @Column(name = "status")
    private String status;

    @Column(name = "grade", nullable = false)
    private long grade;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "course_id", nullable = false)
    private Course course;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "username", nullable = false)
    private User user;


    public Subscribe() {
        this.status = SUBSCRIBER;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public long getGrade() {
        return grade;
    }

    public void setGrade(long grade) {
        this.grade = grade;
    }
}
