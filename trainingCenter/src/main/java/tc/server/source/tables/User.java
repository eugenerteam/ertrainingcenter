package tc.server.source.tables;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by evgeniy.ranchenko on 28.11.2016.
 */
@Entity
@Table(name = "site_users")
public class User {
    public static final String USER="User";
    public static final String LECTURER="Lecturer";
    public static final String KM="Knowlege manager";
    public static final String DM="Department manager";

    @Id
    @Column(name = "username", unique = true, nullable = false, length = 45)
    private String username;

    @Column(name = "password", nullable = false, length = 60)
    private String password;

    @Column(name = "enabled", nullable = false)
    private boolean enabled;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
    private Set<UserRole> userRole = new HashSet<>(0);

    @Column(name = "site_role", nullable = false)
    private String siteRole;

    @Column(name = "email", nullable = false)
    private String email;

    @Column(name = "full_name")
    private String fullname;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
    private Set<Subscribe> subscribes = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
    private Set<CourseApprovement> courseApprovements = new HashSet<>();

    public User(){}

    public User(String username, String password, boolean enabled) {
        this.username = username;
        this.password = password;
        this.enabled = enabled;
    }

    public User(String username, String password, boolean enabled, Set<UserRole> userRole) {
        this.username = username;
        this.password = password;
        this.enabled = enabled;
        this.userRole = userRole;
    }


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public Set<UserRole> getUserRole() {
        return userRole;
    }

    public void setUserRole(Set<UserRole> userRole) {
        this.userRole = userRole;
    }

    public String getSiteRole() {
        return siteRole;
    }

    public void setSiteRole(String siteRole) {
        this.siteRole = siteRole;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Set<Subscribe> getSubscribes() {
        return subscribes;
    }

    public void setSubscribes(Set<Subscribe> subscribes) {
        this.subscribes = subscribes;
    }

    public Set<CourseApprovement> getCourseApprovements() {
        return courseApprovements;
    }

    public void setCourseApprovements(Set<CourseApprovement> courseApprovements) {
        this.courseApprovements = courseApprovements;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }
}
