package tc.server.source.tables;

import javax.persistence.*;

/**
 * Created by evgeniy.ranchenko on 15.12.2016.
 */
@Entity
@Table(name = "events")
public class Event {
    public static final String PROPOSE = "propose";
    public static final String APPROVE_UPD = "approveupd";
    public static final String ADDED = "added";
    public static final String REJECTED = "rejected";
    public static final String DELETED = "deleted";
    public static final String OPEN = "opened";
    public static final String READY = "ready";
    public static final String STARTED = "started";
    public static final String FINISHED = "finished";
    public static final String NOTIFY = "notify";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "event_id")
    private long id;

    @Column(name = "name")
    private String name;

    @Column(name = "recipient")
    private String recipient;

    @Column(name = "copy_recipient")
    private String copyRecipient;

    @Column(name = "template")
    private String template;

    @Column(name = "link_part")
    private String linkPart;

    @Column(name = "subject")
    private String subject;

    public Event() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRecipient() {
        return recipient;
    }

    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    public String getCopyRecipient() {
        return copyRecipient;
    }

    public void setCopyRecipient(String copyRecipient) {
        this.copyRecipient = copyRecipient;
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    public String getLinkPart() {
        return linkPart;
    }

    public void setLinkPart(String linkPart) {
        this.linkPart = linkPart;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }
}
