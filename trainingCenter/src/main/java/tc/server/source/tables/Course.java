package tc.server.source.tables;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by evgeniy.ranchenko on 28.11.2016.
 */
@Entity
@Table(name = "courses")
public class Course {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "course_id")
    private long id;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "links")
    private String links;

    @Column(name = "owner") //just for fast info
    private String owner;

    @Column(name="min_subs_qty")
    private long minSubsQty;

    @Column(name="min_atts_qty")
    private long minAttsQty;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "course", cascade = CascadeType.ALL)
    private Set<Subscribe> subscribes = new HashSet<>();

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "category_id", nullable = false)
    private Category category;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "state_name", nullable = false)
    private CourseState courseState;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "course", cascade = CascadeType.ALL)
    Set<CourseApprovement> courseApprovements = new HashSet<>();

    public Course() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLinks() {
        return links;
    }

    public void setLinks(String links) {
        this.links = links;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public Set<Subscribe> getSubscribes() {
        return subscribes;
    }

    public void setSubscribes(Set<Subscribe> subscribes) {
        this.subscribes = subscribes;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public CourseState getCourseState() {
        return courseState;
    }

    public void setCourseState(CourseState courseState) {
        this.courseState = courseState;
    }

    public Set<CourseApprovement> getCourseApprovements() {
        return courseApprovements;
    }

    public void setCourseApprovements(Set<CourseApprovement> courseApprovements) {
        this.courseApprovements = courseApprovements;
    }

    public long getMinSubsQty() {
        return minSubsQty;
    }

    public void setMinSubsQty(long minSubsQty) {
        this.minSubsQty = minSubsQty;
    }

    public long getMinAttsQty() {
        return minAttsQty;
    }

    public void setMinAttsQty(long minAttsQty) {
        this.minAttsQty = minAttsQty;
    }
}
