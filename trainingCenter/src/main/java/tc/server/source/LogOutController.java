package tc.server.source;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

import javax.servlet.http.HttpSession;

/**
 * Created by User on 26.11.2016.
 */
@Controller
@RequestMapping("/logout")
public class LogOutController {

    @RequestMapping
    public String getLogout(ModelMap model){
        System.out.println("lo");
        return "logout";
    }

    @RequestMapping("/logout1")
    public String proceedLogout(HttpSession session){
        System.out.println("lo1");
        session.invalidate();
        SecurityContextHolder.clearContext();
        return "redirect:/courses";
    }
}
