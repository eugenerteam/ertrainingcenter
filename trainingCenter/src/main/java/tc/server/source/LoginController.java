package tc.server.source;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 25.11.2016.
 */
@Controller
@RequestMapping("/login")
public class LoginController {

    @RequestMapping
    public String getLoginPage(@RequestParam(value = "err", required = false) String err, ModelMap model){
        if (err!=null){
            List<String> errors = new ArrayList<>();
            if (err.equals("EmptyU")||err.equals("EmptyUnP")) errors.add("Username needed");
            if (err.equals("EmptyP")||err.equals("EmptyUnP")) errors.add("Password needed");
            if (err.equals("UserNotFound")) errors.add("Invalid username");
            if (err.equals("BadPass")) errors.add("Invalid password");
            model.addAttribute("errors",errors);
        }
        System.out.println("l");
        return "login";
    }

}
