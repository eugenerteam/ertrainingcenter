package tc.server.source.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import tc.server.source.dao.*;
import tc.server.source.mail.MailService;
import tc.server.source.models.ApproveModel;
import tc.server.source.models.CategoryModel;
import tc.server.source.models.CourseModel;
import tc.server.source.models.EmailModel;
import tc.server.source.tables.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 21.12.2016.
 */
@Component
public class CourseService {

    @Autowired
    private CoursesDao coursesDao;

    @Autowired
    private CategoryDao categoryDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private CourseStateDao courseStateDao;

    @Autowired
    private SubscriptionDao subscriptionDao;

    @Autowired
    private MailService mailService;

    public void addCourse(CourseModel model, String ownerName){
        Course course = new Course();
        course.setOwner(ownerName);
        updateCourse(course,model);
    }

    public void updateCourse(String courseId, CourseModel courseModel){
        Course course = coursesDao.getCourseById(courseId);
        updateCourse(course, courseModel);
    }

    public void updateCourse(Course course, CourseModel model){
        if (course==null) return;
        boolean isNewCourse = (course.getId()==0);
        String oldstate = CourseState.DRAFT;
        if (!isNewCourse) oldstate = course.getCourseState().getName();

        //for partial update
        if (model.getName()!=null) course.setName(model.getName());
        if (model.getDescription()!=null) course.setDescription(model.getDescription());
        if (model.getLinks()!=null)course.setLinks(model.getLinks());
        if (model.getCategoryId()!=0) course.setCategory(categoryDao.getCategoryById(model.getCategoryId()));
        if (model.getCourseState()!=null) course.setCourseState(courseStateDao.getCourseStateByName(model.getCourseState()));
        if (course.getId()==0){
            course.setMinSubsQty(0);
            course.setMinAttsQty(0);
        } else{
            if (model.getMinSubs()!=0) course.setMinSubsQty(model.getMinSubs());
            if (model.getMinAtts()!=0) course.setMinAttsQty(model.getMinAtts());
        }

        coursesDao.updateCourse(course);

        String newstate = course.getCourseState().getName();
        if (!newstate.equals(oldstate)){// we must remember about updetes, that not chenge status
            if (courseStateDao.isItHasStatus(course,CourseState.PROPOSAL)){
                EmailModel emailModel= new EmailModel();
                emailModel.fillForPropose(course);
                mailService.PrepareAndSend(emailModel);
            }else
            if (courseStateDao.isItHasStatus(course,CourseState.OPEN)){
                EmailModel emailModel= new EmailModel();
                emailModel.fillForOpened(course);
                mailService.PrepareAndSend(emailModel);
            }else
            if (courseStateDao.isItHasStatus(course,CourseState.READY)){
                EmailModel emailModel= new EmailModel();
                emailModel.fillForReady(course);
                mailService.PrepareAndSend(emailModel);
            }else
            if (courseStateDao.isItHasStatus(course,CourseState.IN_PROGRESS)){
                EmailModel emailModel= new EmailModel();
                emailModel.fillForStarted(course);
                mailService.PrepareAndSend(emailModel);
            }
            if (courseStateDao.isItHasStatus(course,CourseState.FINISHED)){
                EmailModel emailModel= new EmailModel();
                emailModel.fillForFinished(course);
                mailService.PrepareAndSend(emailModel);
            }
        }
    }

    public void deleteCourse(String courseId){
        Course course = coursesDao.getCourseById(courseId);
        deleteCourse(course);
    }

    public void deleteCourse(Course course){
        if (course==null) return;

        ApproveModel approveModel = new ApproveModel();
        approveModel.fillFromCourse(course);
        EmailModel model = new EmailModel();
        model.fillForDeleted(course,approveModel);
        mailService.PrepareAndSend(model);

        coursesDao.deleteCourse(course);
    }

    public void sendNotify(String courseId){
        Course course = coursesDao.getCourseById(courseId);
        sendNotify(course);
    }

    public void sendNotify(Course course){
        if (course==null) return;
        if (subscriptionDao.getCourseUsersNumberWithStatus(course,Subscribe.ATTENDEE)>0){
            EmailModel emailModel= new EmailModel();
            emailModel.fillForNotify(course);
            mailService.PrepareAndSend(emailModel);
        }
    }

    public void updateApprove(ApproveModel approveModel, String username){
        if (approveModel==null) return;
        Course course = coursesDao.getCourseById(approveModel.getCourseId());
        if (course==null) return;
        coursesDao.updateApprove(course, approveModel);

        EmailModel model = new EmailModel();
        User user = userDao.getByUserName(username);
        approveModel.fillFromCourse(course);
        model.fillForApproveUpd(course,user,approveModel);
        mailService.PrepareAndSend(model);

        if (course.getCourseState().getName().equals(CourseState.NEW)){
            model.fillForAdded(course);
            mailService.PrepareAndSend(model);
        }else if (course.getCourseState().getName().equals(CourseState.REJECTED)){
            model.fillForRejected(course, approveModel);
            mailService.PrepareAndSend(model);
        }

    }

    public void addSubscription(String courseId, String username){
        Course course = coursesDao.getCourseById(courseId);
        User user = userDao.getByUserName(username);
        addSubscription(course, user);
    }

    public void addSubscription(Course course, User user){
        if (course==null) return;
        userDao.addSubscription(course,user);

        long subscrQty = subscriptionDao.getCourseUsersNumberWithStatus(course, Subscribe.SUBSCRIBER);
        if (subscrQty>=course.getMinSubsQty()
                && courseStateDao.isItHasStatus(course, CourseState.NEW)){
            CourseModel model = new CourseModel();
            model.setCourseState(CourseState.OPEN);
            updateCourse(course, model);
        }

    }

    public void addAttend(String courseId, String username){
        Course course = coursesDao.getCourseById(courseId);
        User user = userDao.getByUserName(username);
        addAttend(course, user);
    }

    public void addAttend(Course course, User user){
        userDao.addAttend(course, user);

        long attsQty = subscriptionDao.getCourseUsersNumberWithStatus(course, Subscribe.ATTENDEE);
        if (attsQty>=course.getMinAttsQty()
                && courseStateDao.isItHasStatus(course, CourseState.OPEN)){
            CourseModel model = new CourseModel();
            model.setCourseState(CourseState.READY);
            updateCourse(course, model);
        }
    }

    public void addGrade(String courseId, String username, long grade){
        Course course = coursesDao.getCourseById(courseId);
        User user = userDao.getByUserName(username);
        addGrade(course, user, grade);
    }

    public void addGrade(Course course, User user, long grade){
        userDao.addGrade(course, user, grade);
    }

    public List<CategoryModel> getCategoryList(){
        List<CategoryModel> categoryModelList = new ArrayList<>();
        List <Category> categories = categoryDao.getCategoriesList();
        for (Category c: categories){
            CategoryModel categoryModel = new CategoryModel();
            categoryModel.setName(c.getName());
            categoryModel.setId(c.getId());
            categoryModelList.add(categoryModel);
        }
        return categoryModelList;
    }
}
