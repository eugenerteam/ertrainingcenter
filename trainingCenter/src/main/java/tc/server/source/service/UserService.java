package tc.server.source.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import tc.server.source.dao.CoursesDao;
import tc.server.source.dao.UserDao;
import tc.server.source.tables.Course;
import tc.server.source.tables.Subscribe;
import tc.server.source.tables.User;

/**
 * Created by User on 21.12.2016.
 */
@Component
public class UserService {
    @Autowired
    UserDao userDao;

    //for creating new course
    public boolean isLecturer(String username){
        User user = userDao.getByUserName(username);
        return user.getSiteRole().equals(User.LECTURER);
    }

}
