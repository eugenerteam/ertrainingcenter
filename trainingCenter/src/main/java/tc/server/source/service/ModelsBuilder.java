package tc.server.source.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import tc.server.source.dao.*;
import tc.server.source.models.*;
import tc.server.source.tables.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 20.12.2016.
 */
@Component
public class ModelsBuilder {
    @Autowired
    private CoursesDao courseDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private CourseStateDao courseStateDao;

    @Autowired
    private SubscriptionDao subscriptionDao;

    @Autowired
    private CategoryDao categoryDao;

    public CourseWithStatus buildCourseWithStatus(long courseId, String username){
        Course course = courseDao.getCourseById(courseId);
        return buildCourseWithStatus(course, username);
    }

    public CourseWithStatus buildCourseWithStatus(String courseId, String username){
        Course course = courseDao.getCourseById(courseId);
        return buildCourseWithStatus(course, username);
    }

    public CourseWithStatus buildCourseWithStatus(String courseId, User user){
        Course course = courseDao.getCourseById(courseId);
        return buildCourseWithStatus(course, user);

    }

    public CourseWithStatus buildCourseWithStatus(Course course, String username){
        User user = userDao.getByUserName(username);
        return buildCourseWithStatus(course, user);
    }

    public CourseWithStatus buildCourseWithStatus(Course course, User user){
        if (course==null) return null;
        CourseWithStatus courseWithStatus = new CourseWithStatus();
        courseWithStatus.setCourse(course);
        Subscribe subscribe = subscriptionDao.getSubscribeByUserAndCourse(user,course);
        if (subscribe!=null) courseWithStatus.setUserStatusInCourse(subscribe.getStatus());
        courseWithStatus.setUserRole(user.getSiteRole());
        courseWithStatus.setCourseStatus(course.getCourseState().getName());
        courseWithStatus.setSubscribersCount(subscriptionDao.getCourseUsersNumberWithStatus(course, Subscribe.SUBSCRIBER));
        courseWithStatus.setAttendeeCount(subscriptionDao.getCourseUsersNumberWithStatus(course, Subscribe.ATTENDEE));
        courseWithStatus.setGrade((float) subscriptionDao.getCourseAverageGrade(course));
        courseWithStatus.setActions();
        return courseWithStatus;
    }

    public CourseModel buildCourseModel(String courseId, CourseModel courseModel){
        Course course = courseDao.getCourseById(courseId);
        return buildCourseModel(course,courseModel);
    }

    public CourseModel buildCourseModel(Course course, CourseModel courseModel){
        if (course!=null){
            if (courseModel==null) courseModel = new CourseModel();
            courseModel = new CourseModel();
            courseModel.setName(course.getName());
            courseModel.setDescription(course.getDescription());
            courseModel.setLinks(course.getLinks());
            courseModel.setCategoryId(course.getCategory().getId());
            courseModel.setCategoryName(course.getCategory().getName());
            courseModel.setCourseState(course.getCourseState().getName());
            courseModel.setMinSubs(course.getMinSubsQty());
            courseModel.setMinAtts(course.getMinAttsQty());
            courseModel.setOwner(course.getOwner());
        }
        return courseModel;
    }

    public CourseModel buildModelNewCourse(CourseModel courseModel){
        if (courseModel==null) courseModel = new CourseModel();
        courseModel.setName("");
        courseModel.setDescription("");
        courseModel.setLinks("");
        courseModel.setCategoryId(0);
        courseModel.setCourseState(CourseState.DRAFT);
        courseModel.setMinSubs(1); //just for bindins. we clear it during update course
        courseModel.setMinAtts(1);
        return courseModel;
    }

    public CourseModel buildCourseModelByCWS(CourseWithStatus courseWithStatus, CourseModel courseModel){
        if (courseWithStatus==null || courseWithStatus.getCourse()==null) return null;
        if (courseModel==null) courseModel=new CourseModel();
        Course course = courseWithStatus.getCourse();
        courseModel.setName(course.getName());
        courseModel.setDescription(course.getDescription());
        courseModel.setLinks(course.getLinks());
        courseModel.setCategoryId(course.getCategory().getId());
        courseModel.setCategoryName(course.getCategory().getName());
        courseModel.setCourseState(course.getCourseState().getName());
        courseModel.setMinSubs(course.getMinSubsQty());
        courseModel.setMinAtts(course.getMinAttsQty());
        courseModel.setOwner(course.getOwner());
        return courseModel;
    }

    public SubscribeModel buildSubscribeModelByCWS(CourseWithStatus courseWithStatus, SubscribeModel subscribeModel){
        if (courseWithStatus==null || courseWithStatus.getCourse()==null) return null;
        if (subscribeModel==null) subscribeModel=new SubscribeModel();
        subscribeModel.setCourseId(Long.toString(courseWithStatus.getCourse().getId()));
        subscribeModel.setName(courseWithStatus.getCourse().getName());
        subscribeModel.setOwner(courseWithStatus.getCourse().getOwner());
        return subscribeModel;
    }

    public List<CourseWithStatus> getCoursesWithStatusList(String username, long categoryId){
        List<CourseWithStatus> coursesWithStatus = new ArrayList<>();
        User user = userDao.getByUserName(username);
        Category category = categoryDao.getCategoryById(categoryId);
        List<Course> courses = courseDao.getCoursesList(category);
        if (courses!=null){
            for (Course course: courses){
                CourseWithStatus courseWithStatus = buildCourseWithStatus(course,user);
                if (courseWithStatus.isVisible())
                    coursesWithStatus.add(courseWithStatus);
            }
        }
        return coursesWithStatus;
    }

    public List<CourseWithStatus> getUserCoursesWithStatusList(String username, long categoryId){
        List<CourseWithStatus> coursesWithStatus = new ArrayList<>();
        User user = userDao.getByUserName(username);
        Category category = categoryDao.getCategoryById(categoryId);
        List<Course> courses = subscriptionDao.getUserCourses(user, category);
        if (courses!=null){
            for (Course course: courses)
                coursesWithStatus.add(buildCourseWithStatus(course, user));
        }
        return coursesWithStatus;
    }

    public CourseWithParticipants getCourseWithParticipants(String courseId){
        return courseDao.getCourseWithParticipant(courseId);
    }

    public ApproveModel buildApproveModelByCWS(CourseWithStatus courseWithStatus, ApproveModel approveModel){
        if (approveModel==null) approveModel = new ApproveModel();
        approveModel.fillFromCourse(courseWithStatus.getCourse());
        approveModel.setDM(courseWithStatus.isDM());
        approveModel.setKM(courseWithStatus.isKM());
        approveModel.setDMName(userDao.findUserByRole(User.DM).getFullname());
        approveModel.setKMName(userDao.findUserByRole(User.KM).getFullname());
        return approveModel;
    }

}
