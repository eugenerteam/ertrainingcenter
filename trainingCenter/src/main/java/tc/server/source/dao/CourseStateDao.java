package tc.server.source.dao;

import tc.server.source.models.CourseWithStatus;
import tc.server.source.tables.Course;
import tc.server.source.tables.CourseState;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by User on 11.12.2016.
 */
@Component
public class CourseStateDao {
    @Autowired
    HibernateProcessor hibernateProcessor;

    public List<CourseState> getCategoriesList(){
        Session session = hibernateProcessor.getSession();
        List<CourseState> list = session.createQuery("from "+CourseState.class.getName()).list();
        session.close();
        return list;
    }

    public CourseState getCourseStateByName(String name){
        Session session = hibernateProcessor.getSession();
        CourseState courseState = session.get(CourseState.class,name);
        session.close();
        return courseState;
    }

    public boolean isItHasStatus(Course course,String courseState){
        if (course==null || courseState==null) return false;
        else return courseState.equals(course.getCourseState().getName());

    }
}
