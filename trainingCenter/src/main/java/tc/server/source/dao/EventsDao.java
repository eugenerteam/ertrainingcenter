package tc.server.source.dao;

import tc.server.source.tables.Event;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by evgeniy.ranchenko on 15.12.2016.
 */
@Component
public class EventsDao {
    @Autowired HibernateProcessor hibernateProcessor;

    public Event getEventByName(String name){
        Session session = hibernateProcessor.getSession();
        Query query = session.createQuery("from "+Event.class.getName()+" e where e.name=:name");
        query.setParameter("name",name);
        Event event = (Event) query.uniqueResult();
        return event;
    }
}
