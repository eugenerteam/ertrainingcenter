package tc.server.source.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by evgeniy.ranchenko on 28.11.2016.
 */
@Component
public class HibernateProcessor {

    @Autowired
    private SessionFactory sessionFactory;

    public Session getSession(){
        return sessionFactory.openSession();
    }
}
