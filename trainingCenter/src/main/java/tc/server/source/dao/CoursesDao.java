package tc.server.source.dao;

import tc.server.source.mail.MailService;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import tc.server.source.models.*;
import tc.server.source.service.ModelsBuilder;
import tc.server.source.tables.*;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by evgeniy.ranchenko on 28.11.2016.
 */
@Component
public class CoursesDao {

    private HibernateProcessor hibernateProcessor;

    @Autowired
    private UserDao userDao;

    @Autowired
    private CategoryDao categoryDao;
    @Autowired
    private CourseStateDao courseStateDao;

    @Autowired
    private ModelsBuilder modelsBuilder;

    @Autowired
    private  MailService mailService;

    @Autowired
    public CoursesDao(HibernateProcessor hibernateProcessor){
        this.hibernateProcessor = hibernateProcessor;
    }

    public List<Course> getCoursesList(Category category){
        Session session = hibernateProcessor.getSession();
        String queryText =  "from "+Course.class.getName();
        if (category!=null) queryText +=  " c where c.category=:category";
        Query query = session.createQuery(queryText);
        if (category!=null) query.setParameter("category",category);
        List<Course> courseList = (List<Course>) query.list();
        session.close();
        return courseList;
    }

    @Transactional(readOnly = true)
    public CourseWithParticipants getCourseWithParticipant(String courseId){
        CourseWithParticipants courseWithParticipants = new CourseWithParticipants();
        long lid=0;
        try {
            lid = Long.parseLong(courseId);
        }catch (NumberFormatException e){}

        Session session= hibernateProcessor.getSession();
        Course course = session.get(Course.class,lid);
        if (course!=null){
            courseWithParticipants.setCourse(course);
            List<String> subscribers = new ArrayList<>();
            List<String> attendees = new ArrayList<>();

            Set<Subscribe> subscribes= course.getSubscribes();
            for (Subscribe s : subscribes){
                if (s.ATTENDEE.equals(s.getStatus())) attendees.add(s.getUser().getUsername());
                if (s.SUBSCRIBER.equals(s.getStatus())) subscribers.add(s.getUser().getUsername());
            }
            courseWithParticipants.setAttendees(attendees);
            courseWithParticipants.setSubscribers(subscribers);
        }
        session.close();
        return  courseWithParticipants;
    }

    public Course getCourseById(long id){
        Session session = hibernateProcessor.getSession();
        Course course = session.get(Course.class,id);
        session.close();
        return course;
    }

    public Course getCourseById(String id){
        long lid=0;
        try {
            lid = Long.parseLong(id);
        }catch (NumberFormatException e){}
        return getCourseById(lid);
    }


    public void updateCourse(Course course){
        if (course==null) return;
        boolean isNewCourse = (course.getId()==0);

        User owner = userDao.getByUserName(course.getOwner());
        Session session = hibernateProcessor.getSession();
        Transaction tr = null;
        try{
            tr = session.getTransaction();
            tr.begin();
            session.saveOrUpdate(course);
            if (isNewCourse){
                Subscribe subscribe = new Subscribe();
                subscribe.setCourse(course);
                subscribe.setUser(owner);
                subscribe.setStatus(subscribe.OWNER);
                session.saveOrUpdate(subscribe);
            }
            if (courseStateDao.isItHasStatus(course,CourseState.PROPOSAL)){
                Set<CourseApprovement> courseApprovements = course.getCourseApprovements();
                if (courseApprovements.size()!=0)
                    for (CourseApprovement courseApprovement : courseApprovements) {
                        courseApprovement.setStatus(0);
                        session.saveOrUpdate(courseApprovement);
                    }
                else{ //no approvements
                    CourseApprovement courseApprovement = new CourseApprovement();
                    courseApprovement.setStatus(0);
                    courseApprovement.setCourse(course);
                    courseApprovement.setUser(userDao.findUserByRole(User.KM));
                    courseApprovement.setType(courseApprovement.TYPE_KM);
                    session.saveOrUpdate(courseApprovement);

                    courseApprovement = new CourseApprovement();
                    courseApprovement.setStatus(0);
                    courseApprovement.setCourse(course);
                    courseApprovement.setUser(userDao.findUserByRole(User.DM));
                    courseApprovement.setType(courseApprovement.TYPE_DM);
                    session.saveOrUpdate(courseApprovement);
                }
            }
            tr.commit();
        }catch (RuntimeException ex){
            if (tr!=null) tr.rollback();
        }finally {
            session.close();
        }
    }

    public void updateApprove(Course course, ApproveModel approveModel){
        if (course==null) return;
        Session session = hibernateProcessor.getSession();
        Transaction tr = null;
        try{
            tr = session.getTransaction();
            tr.begin();
            Set<CourseApprovement> courseApprovements = course.getCourseApprovements();
            long result =0;
            long votes = 0;

            for (CourseApprovement courseApprovement : courseApprovements){
                if (courseApprovement.getType()==CourseApprovement.TYPE_DM && approveModel.isDM()){
                    courseApprovement.setReason(approveModel.getDMNote());
                    courseApprovement.setStatus(approveModel.getDMStatus());
                    session.saveOrUpdate(courseApprovement);
                }else if (courseApprovement.getType()==CourseApprovement.TYPE_KM && approveModel.isKM()){
                    courseApprovement.setReason(approveModel.getKMNote());
                    courseApprovement.setStatus(approveModel.getKMStatus());
                    session.saveOrUpdate(courseApprovement);
                }
                if (courseApprovement.getStatus()!=0) votes++;
                result+=courseApprovement.getStatus();
            }
            if (result==2){
                course.setCourseState(courseStateDao.getCourseStateByName(CourseState.NEW));
            }
            else if (votes==2) {
                course.setCourseState(courseStateDao.getCourseStateByName(CourseState.REJECTED));
            }
            session.saveOrUpdate(course);
            tr.commit();
        }catch (RuntimeException ex){
            if (tr!=null) tr.rollback();
        }
        session.close();

    }

    public void deleteCourse(Course course){
        if (course == null) return;

        Session session = hibernateProcessor.getSession();
        Transaction tr = null;
        try{
            tr = session.getTransaction();
            tr.begin();

            session.delete(course);
            tr.commit();
        }catch (RuntimeException ex){
            ex.printStackTrace();
            if (tr!=null) tr.rollback();
        }
        session.close();
    }
}
