package tc.server.source.dao;

import tc.server.source.tables.Category;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by User on 11.12.2016.
 */
@Component
public class CategoryDao {
    @Autowired
    HibernateProcessor hibernateProcessor;

    public List<Category> getCategoriesList(){
        Session session = hibernateProcessor.getSession();
        List<Category> list = session.createQuery("from "+Category.class.getName()).list();
        session.close();
        return list;
    }

    public Category getCategoryById(long id){
        Session session = hibernateProcessor.getSession();
        Category category = session.get(Category.class,id);
        session.close();
        return category;
    }

    public Category getCategoryById(String id){
        long lid=0;
        try {
            lid = Long.parseLong(id);
        }catch (NumberFormatException e){}
        return getCategoryById(lid);
    }

}
