package tc.server.source.dao;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import tc.server.source.tables.Course;
import tc.server.source.tables.Subscribe;
import tc.server.source.tables.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by evgeniy.ranchenko on 28.11.2016.
 */
@Repository
public class UserDao {


    @Autowired
    private HibernateProcessor hibernateProcessor;

    @Autowired
    SubscriptionDao subscriptionDao;

    @Autowired CourseStateDao courseStateDao;

    @Autowired CoursesDao coursesDao;

    //for authentication
    public User findByUserName(String username) { //for authentication
        Session session = hibernateProcessor.getSession();
        User user = (User) session.get(User.class,username);
        return user;

    }

    public User getByUserName(String username) {
        Session session = hibernateProcessor.getSession();
        User user = (User) session.get(User.class,username);
        session.close();
        return user;

    }

    public User findUserByRole(String role){
        Session session = hibernateProcessor.getSession();
        String queryText = "from "+User.class.getName()+" u where u.siteRole=:role";
        Query query = session.createQuery(queryText);
        query.setParameter("role",role);
        User user = (User) query.uniqueResult();
        session.close();
        return user;
    }

    public List<String> getUsenamesByRoleMask(String roleMask){
        List<String> result = null;
        Session session = hibernateProcessor.getSession();
        String queryText = "from "+User.class.getName()+" u where u.siteRole like :role";
        Query query = session.createQuery(queryText);
        roleMask = roleMask.substring(5); //after "role:"
        query.setParameter("role",roleMask);
        List<User> users = (List<User>) query.list();
        if (users!=null || users.size()>0){
            result = new ArrayList<>();
            for (User user : users) result.add(user.getEmail());
        }
        session.close();


        return result;
    }

    public void addSubscription(Course course , User user){

        Session session = hibernateProcessor.getSession();
        Transaction tr = null;
        try{
            tr = session.getTransaction();
            tr.begin();
            if (subscriptionDao.getSubscribeByUserAndCourse(user,course)==null){
                Subscribe subscribe = new Subscribe();
                subscribe.setCourse(course);
                subscribe.setUser(user);
                subscribe.setStatus(Subscribe.SUBSCRIBER);
                session.saveOrUpdate(subscribe);
            }
            tr.commit();
        }catch (RuntimeException ex){
            if (tr!=null) tr.rollback();
        }
        session.close();
    }

    public void addAttend(Course course, User user){
        if (course!=null) {
            Session session = hibernateProcessor.getSession();
            Subscribe subscribe = subscriptionDao.getSubscribeByUserAndCourse(user, course);

            if (subscribe!=null){
                subscribe.setStatus(Subscribe.ATTENDEE);

                Transaction tr = null;
                try{
                    tr = session.getTransaction();
                    tr.begin();
                    session.saveOrUpdate(subscribe);
                    tr.commit();
                }catch (RuntimeException ex){
                    if (tr!=null) tr.rollback();
                }
            }
            session.close();
        }
    }

    public void addGrade(Course course, User user, long grade){
        if (course!=null) {
            Session session = hibernateProcessor.getSession();
            Subscribe subscribe = subscriptionDao.getSubscribeByUserAndCourse(user, course);

            if (subscribe!=null){
                subscribe.setGrade(grade);
                subscribe.setStatus(subscribe.GRADUATED);

                Transaction tr = null;
                try{
                    tr = session.getTransaction();
                    tr.begin();
                    session.saveOrUpdate(subscribe);
                    tr.commit();
                }catch (RuntimeException ex){
                    if (tr!=null) tr.rollback();
                }
            }
            session.close();
        }
    }


    @Transactional(readOnly = true)
    public boolean isItHasStatus(Course course, User user, String status){
        boolean result =false;
        if (course!=null) {
            Session session = hibernateProcessor.getSession();
            String queryText = "from "+Subscribe.class.getName()+" s where s.status=:status and s.user=:user and s.course=:course";
            Query query = session.createQuery(queryText);
            query.setParameter("status",status);
            query.setParameter("user",user);
            query.setParameter("course",course);
            Subscribe s = (Subscribe) query.uniqueResult();
            if (s!=null) result = true;
            session.close();
        }
        return result;
    }

}
