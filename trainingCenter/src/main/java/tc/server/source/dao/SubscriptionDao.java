package tc.server.source.dao;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import tc.server.source.models.CourseWithStatus;
import tc.server.source.service.ModelsBuilder;
import tc.server.source.tables.Category;
import tc.server.source.tables.Course;
import tc.server.source.tables.Subscribe;
import tc.server.source.tables.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by evgeniy.ranchenko on 08.12.2016.
 */
@Component
public class SubscriptionDao {
    @Autowired
    private HibernateProcessor hibernateProcessor;

    @Autowired private UserDao userDao;
    @Autowired private CourseStateDao courseStateDao;
    @Autowired
    private ModelsBuilder modelsBuilder;

    @Transactional(readOnly = true)
    public long getCourseUsersNumberWithStatus(Course course, String status){
        long result=0;
        Session session = hibernateProcessor.getSession();
        if (course!=null){
            String queryText = "select count(s.user) from "+Subscribe.class.getName()+" s where s.course=:course and s.status=:status";
            Query query= session.createQuery(queryText);
            query.setParameter("course",course);
            query.setParameter("status",status);
            result = (Long) query.uniqueResult();
        }
        session.close();
        return result;
    }

    @Transactional(readOnly = true)
    public double getCourseAverageGrade(Course course){
        double result=0;
        Session session = hibernateProcessor.getSession();
        if (course!=null){
            String queryText = "select avg(s.grade) from "+Subscribe.class.getName()+" s where s.course=:course and s.status=:status";
            Query query= session.createQuery(queryText);
            query.setParameter("course",course);
            query.setParameter("status",Subscribe.GRADUATED);
            Double res = (Double) query.uniqueResult();
            if (res!=null) result = res.doubleValue();
        }
        session.close();
        return result;
    }

    public List<Course> getUserCourses(User user, Category category){
        Session session = hibernateProcessor.getSession();
        String queryText = "select s.course from "+Subscribe.class.getName()+" s where s.user=:user";
        if (category!=null) queryText+=" and s.course.category=:category";
        Query query = session.createQuery(queryText);
        query.setParameter("user",user);
        if (category!=null) query.setParameter("category", category);
        List<Course> courses = query.list();
        session.close();
        return  courses;
    }

    public List<String> getUsenamesByUserTypeMask(String typeMask, long courseID){
        List<String> result = null;
        Session session = hibernateProcessor.getSession();

        String queryText = "from "+Subscribe.class.getName()+" s where s.status like :type and s.course.id=:id";
        Query query = session.createQuery(queryText);
        typeMask = typeMask.substring(5); //after "type:"
        query.setParameter("type",typeMask);
        query.setParameter("id",courseID);
        List<Subscribe> subscribes = (List<Subscribe>) query.list();
        if (subscribes!= null || subscribes.size()>0) {
            result = new ArrayList<>();
            for (Subscribe subscribe : subscribes) result.add(subscribe.getUser().getEmail());
        }

        session.close();

        return result;
    }

    public Subscribe getSubscribeByUserAndCourse(User user, Course course){
        Session session = hibernateProcessor.getSession();
        String queryText = "from "+Subscribe.class.getName()+" s where s.user=:user and s.course=:course";
        Query query = session.createQuery(queryText);
        query.setParameter("user",user);
        query.setParameter("course",course);
        Subscribe subscribe = (Subscribe) query.uniqueResult();

        session.close();
        return  subscribe;
    }
}
