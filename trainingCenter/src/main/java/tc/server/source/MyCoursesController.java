package tc.server.source;

import tc.server.source.models.CategoryModel;
import tc.server.source.models.CourseWithStatus;
import tc.server.source.service.ModelsBuilder;
import tc.server.source.service.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;
import java.util.List;

/**
 * Created by User on 11.12.2016.
 */
@Controller
@RequestMapping("/mycourses")
public class MyCoursesController {

    @Autowired
    CourseService courseService;

    @Autowired
    ModelsBuilder modelsBuilder;

    @RequestMapping
    public String getMyCourses(Principal principal, CategoryModel categoryModel, ModelMap model){

        model.addAttribute("categoryList",courseService.getCategoryList());
        if (categoryModel.getId()==0){
            model.addAttribute("categoryModel",categoryModel);
        }
        List<CourseWithStatus> list = modelsBuilder.getUserCoursesWithStatusList(principal.getName(),categoryModel.getId());
        model.addAttribute("coursesList",list);
        return "mycourses";
    }
}
