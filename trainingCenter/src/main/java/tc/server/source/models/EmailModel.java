package tc.server.source.models;

import tc.server.source.tables.Course;
import tc.server.source.tables.Event;
import tc.server.source.tables.User;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by User on 12.12.2016.
 */
public class EmailModel {

    public static final String LECTOR_NAME = "LectorName";
    public static final String COURSE_ID = "CourseId";
    public static final String COURSE_NAME = "CourseName";
    public static final String COURSE_CATEGORY = "CourseCategory";
    public static final String COURSE_DESCRIPTION = "CourseDescription";
    public static final String COURSE_LINKS = "CourseLinks";
    public static final String ACTION_TYPE = "actionType";
    public static final String EVENT = "Event";
    public static final String VOTES = "vote";

    private Map<String,Object> map = new HashMap<>();

    public void put(String name, Object object){
        map.put(name,object);
    }

    public Object get(String name){
        return map.get(name);
    }

    public Object remove(String name){
        return map.remove(name);
    }

    public void clearModel(){
        map.clear();
    }

    public Map<String,Object> getAsMap(){
        return map;
    }

    public void fillFromCourse(Course course){
        clearModel();
        map.put(LECTOR_NAME,course.getOwner());
        map.put(COURSE_ID,course.getId());
        map.put(COURSE_NAME,course.getName());
        map.put(COURSE_CATEGORY, course.getCategory().getName());
        map.put(COURSE_DESCRIPTION, course.getDescription());
        map.put(COURSE_LINKS,course.getLinks());

    }

    public void fillForPropose(Course course){
        fillFromCourse(course);
        map.put(ACTION_TYPE,"approve");
        map.put(EVENT, Event.PROPOSE);
    }

    public void fillForApproveUpd(Course course, User user, ApproveModel model){
        fillFromCourse(course);
        map.put(EVENT, Event.APPROVE_UPD);
        map.put("ManagerName",user.getUsername());

        long status = (model.isDM())? model.getDMStatus():model.getKMStatus();
        String decision = "no decision";
        if (status==1) decision="Course approved";
        else decision="Course rejected";
        String reason = (model.isDM())? model.getDMNote():model.getKMNote();

        map.put("ManagerDecision",decision);
        map.put("ManagerReason",reason);
    }

    public void fillForAdded(Course course){
        fillFromCourse(course);
        map.put(EVENT, Event.ADDED);
        map.put(ACTION_TYPE,"subscribe");

    }

    public void fillForOpened(Course course){
        fillFromCourse(course);
        map.put(EVENT, Event.OPEN);
        map.put(ACTION_TYPE,"attend");
    }

    public void fillForReady(Course course){
        fillFromCourse(course);
        map.put(EVENT, Event.READY);
    }

    public void fillForStarted(Course course){
        fillFromCourse(course);
        map.put(EVENT, Event.STARTED);
    }

    public void fillForFinished(Course course){
        fillFromCourse(course);
        map.put(EVENT, Event.FINISHED);
    }

    public void fillForNotify(Course course){
        fillFromCourse(course);
        map.put(EVENT, Event.NOTIFY);
    }

    public void fillForRejected(Course course, ApproveModel model){
        fillFromCourse(course);
        String decision = "no decision";
        long votes=0;
        map.put(EVENT, Event.REJECTED);

        map.put(ACTION_TYPE,"update");
        //DM
        map.put("ManagerName",model.getDMName());
        if (model.getDMStatus()==1) decision="Course approved";
        else {
            decision="Course rejected";
            votes++;
        }

        map.put("ManagerDecision",decision);
        map.put("ManagerReason",model.getDMNote());

        //KM
        map.put("ManagerName2",model.getKMName());

        if (model.getKMStatus()==1) decision="Course approved";
        else {
            decision="Course rejected";
            votes++;
        }

        map.put("ManagerDecision2",decision);
        map.put("ManagerReason2",model.getKMNote());
        if (votes==1) map.put(VOTES,"1 vote");
        else map.put(VOTES,"2 votes");
    }

    public void fillForDeleted(Course course, ApproveModel model){
        fillFromCourse(course);
        map.put(EVENT, Event.DELETED);
        String decision = "no decision";
        //DM
        map.put("ManagerName",model.getDMName());
        if (model.getDMStatus()==1) decision="Course approved";
        else decision="Course rejected";

        map.put("ManagerDecision",decision);
        map.put("ManagerReason",model.getDMNote());

        //KM
        map.put("ManagerName2",model.getKMName());

        if (model.getKMStatus()==1) decision="Course approved";
        else decision="Course rejected";

        map.put("ManagerDecision2",decision);
        map.put("ManagerReason2",model.getKMNote());
    }
}
