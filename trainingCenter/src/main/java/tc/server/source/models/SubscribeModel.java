package tc.server.source.models;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * Created by evgeniy.ranchenko on 08.12.2016.
 * used for subscription, attends and evaluates
 */
public class SubscribeModel {

    @NotEmpty
    protected String courseId;

    protected String name;

    protected String owner;

    public String getCourseId() {
        return courseId;
    }

    public void setCourseId(String courseId) {
        this.courseId = courseId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }
}
