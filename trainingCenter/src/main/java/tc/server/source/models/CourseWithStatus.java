package tc.server.source.models;

import tc.server.source.tables.Course;
import tc.server.source.tables.CourseState;
import tc.server.source.tables.Subscribe;
import tc.server.source.tables.User;

/**
 * Created by evgeniy.ranchenko on 08.12.2016.
 */
public class CourseWithStatus {
    private Course course;
    //user states - owner, attendee...
    String userStatusInCourse ="";

    //role - lecturer, user etc.
    String userRole="";

    //course states: new, rejected...
    private String courseStatus="";
    //counters
    private long subscribersCount;
    private long attendeeCount;
    private float grade;
    public String action=null;
    public String text=""; //text for button
    public String label="label";

    public void init(Course course, User user){
    }

    public void setActions(){
        if (isUser()){
            if (isNew() || isOpen() || isReady() ){
                action="subscribe";
                text="Subscribe"; //text for button
            }

            if (isGraduated()){
                action = null;
                text = "";
            }

            if (isAttandee()) {
                if (isFinished()){
                    action = "evaluate";
                    text = "Evaluate";
                }else{
                    action = null;
                    text = "";
                }
            }

            if (isSubscriber()){
                if (isOpen()||isReady()){
                    action = "attend";
                    text = "Attend";
                }else if (isNew()){
                    action = null;
                    text = "";
                }
            }
        }else if (isOwner() && isProposal()){
            action=null;
            text="";
        }else if (isLecturer()){
            action = "update";
            text="Update";
        }else if(isProposal() && (isDM()||isKM())){
            action = "approve";
            text="Approve";
        }
        if (isProposal()) label="label label-warning";
        if (isRejected()) label="label label-important";
        if (isNew()||isOpen()||isReady()) label="label label-success";
        if (isInProgress()) label="label label-inverse";
    }

    public boolean isParticipant(){
        return isAttandee()||isSubscriber()||isGraduated();
    }

    public boolean isVisible(){
        boolean result = true;
        if (isDraft() && !isOwner()) result = false;
        else if (isProposal() && !(isKM() || isDM() || isOwner())) result = false;
        else if (isRejected() && !(isKM() || isDM() || isOwner())) result = false;
        return  result;
    }

    public boolean isEditable(){
        return (isOwner()&&(isDraft()||isRejected()));
    }

    public boolean isAvailableForSubscribe(){
        return (isUser()&&(isNew()||isOpen()||isReady())&&!isParticipant());
    }

    public boolean isAvailableForAttend(){
        return ((isOpen()||isReady())&&isSubscriber());
    }

    public boolean isAvailableForEvaluate(){
        return (isFinished()&&isAttandee());
    }

//-------------------------------------

    public Course getCourse() {
        return course;
    }

    public boolean isSubscriber() {
        return userStatusInCourse.equals(Subscribe.SUBSCRIBER);
    }

    public boolean isAttandee() {
        return userStatusInCourse.equals(Subscribe.ATTENDEE);
    }

    public boolean isGraduated() {
        return userStatusInCourse.equals(Subscribe.GRADUATED);
    }

    public boolean isOwner() {
        return userStatusInCourse.equals(Subscribe.OWNER);
    }

//-----------------------------
    public String getAction() {
        return action;
    }

    public String getText() {
        return text;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public void setText(String text) {
        this.text = text;
    }

    public long getSubscribersCount() {
        return subscribersCount;
    }

    public void setSubscribersCount(long subscribersCount) {
        this.subscribersCount = subscribersCount;
    }

    public long getAttendeeCount() {
        return attendeeCount;
    }

    public void setAttendeeCount(long attendeeCount) {
        this.attendeeCount = attendeeCount;
    }

    public float getGrade() {
        return grade;
    }

//------------------

    public void setGrade(float grade) {
        this.grade = grade;
    }

    public boolean isDraft() {
        return courseStatus.equals(CourseState.DRAFT);
    }

    public boolean isProposal() {
        return courseStatus.equals(CourseState.PROPOSAL);
    }

    public boolean isRejected() {
        return courseStatus.equals(CourseState.REJECTED);
    }

    public boolean isNew() {
        return courseStatus.equals(CourseState.NEW);
    }

    public boolean isOpen() {
        return courseStatus.equals(CourseState.OPEN);
    }

    public boolean isReady() {
        return courseStatus.equals(CourseState.READY);
    }

    public boolean isInProgress() {
        return courseStatus.equals(CourseState.IN_PROGRESS);
    }

    public boolean isFinished() {
        return courseStatus.equals(CourseState.FINISHED);
    }

//---------------------------

    public boolean isLecturer() {
        return userRole.equals(User.LECTURER);
    }

    public boolean isKM() {
        return userRole.equals(User.KM);
    }

    public boolean isDM() {
        return userRole.equals(User.DM);
    }

    public boolean isUser() {
        return userRole.equals(User.USER);
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getUserStatusInCourse() {
        return userStatusInCourse;
    }

    public void setUserStatusInCourse(String userStatusInCourse) {
        this.userStatusInCourse = userStatusInCourse;
    }

    public String getUserRole() {
        return userRole;
    }

    public void setUserRole(String userRole) {
        this.userRole = userRole;
    }

    public String getCourseStatus() {
        return courseStatus;
    }

    public void setCourseStatus(String courseStatus) {
        this.courseStatus = courseStatus;
    }
}
