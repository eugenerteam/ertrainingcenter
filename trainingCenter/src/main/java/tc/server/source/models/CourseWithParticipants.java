package tc.server.source.models;

import tc.server.source.tables.Course;

import java.util.List;

/**
 * Created by User on 10.12.2016.
 */
public class CourseWithParticipants {
    private Course course;
    private List<String> subscribers;
    private List<String> attendees;
    private boolean hasSubscribers;
    private boolean hasAttendees;

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public List<String> getSubscribers() {
        return subscribers;
    }

    public void setSubscribers(List<String> subscribers) {

        this.subscribers = subscribers;
        this.hasSubscribers = false;
        if (this.subscribers!=null)
            if (this.subscribers.size()>0) hasSubscribers=true;
    }

    public List<String> getAttendees() {
        return attendees;
    }

    public void setAttendees(List<String> attendees) {
        this.attendees = attendees;
        this.hasAttendees = false;
        if (this.attendees!=null)
            if (this.attendees.size()>0) hasAttendees=true;
    }

    public boolean isHasSubscribers() {
        return hasSubscribers;
    }

    public boolean isHasAttendees() {
        return hasAttendees;
    }
}
