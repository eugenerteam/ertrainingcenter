package tc.server.source.models;

import tc.server.source.tables.Course;
import tc.server.source.tables.CourseApprovement;

import java.util.Set;

/**
 * Created by evgeniy.ranchenko on 13.12.2016.
 */
public class ApproveModel {
    private long courseId;
    private boolean isDM;
    private boolean isKM;
    private long KMStatus;
    private long DMStatus;
    private String KMNote;
    private String DMNote;
    private String KMName;
    private String DMName;

    public void fillFromCourse(Course course){
        this.courseId = course.getId();

        Set<CourseApprovement> courseApprovements = course.getCourseApprovements();
        for (CourseApprovement courseApprovement : courseApprovements)
            if (courseApprovement.getType()==CourseApprovement.TYPE_KM){
                KMStatus = courseApprovement.getStatus();
                KMNote = courseApprovement.getReason();
            }else if (courseApprovement.getType()==CourseApprovement.TYPE_DM){
                DMStatus = courseApprovement.getStatus();
                DMNote = courseApprovement.getReason();
            }
    }

    public long getCourseId() {
        return courseId;
    }

    public void setCourseId(long courseId) {
        this.courseId = courseId;
    }

    public boolean isDM() {
        return isDM;
    }

    public void setDM(boolean DM) {
        isDM = DM;
    }

    public boolean isKM() {
        return isKM;
    }

    public void setKM(boolean KM) {
        isKM = KM;
    }

    public long getKMStatus() {
        return KMStatus;
    }

    public void setKMStatus(long KMStatus) {
        this.KMStatus = KMStatus;
    }

    public long getDMStatus() {
        return DMStatus;
    }

    public void setDMStatus(long DMStatus) {
        this.DMStatus = DMStatus;
    }

    public String getKMNote() {
        return KMNote;
    }

    public void setKMNote(String KMNote) {
        this.KMNote = KMNote;
    }

    public String getDMNote() {
        return DMNote;
    }

    public void setDMNote(String DMNote) {
        this.DMNote = DMNote;
    }

    public String getKMName() {
        return KMName;
    }

    public void setKMName(String KMName) {
        this.KMName = KMName;
    }

    public String getDMName() {
        return DMName;
    }

    public void setDMName(String DMName) {
        this.DMName = DMName;
    }
}
