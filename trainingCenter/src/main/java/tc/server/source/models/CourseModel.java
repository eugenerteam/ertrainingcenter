package tc.server.source.models;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

/**
 * Created by evgeniy.ranchenko on 30.11.2016.
 */
public class CourseModel {

    @NotEmpty(message = "You must fill course name!")
    private String name;

    @NotEmpty(message = "You must fill course description!")
    private String description;

    @NotEmpty(message = "You must fill course links!")
    private String links;

    @Min(value = 1,message = "You must choose category")
    private long categoryId;
    private String categoryName;

    @Min(value = 1, message ="min. subcribers quantity must be positive number between 1 and 10")
    @Max(value = 10,message ="min. subcribers quantity must be positive number between 1 and 10")
    private long minSubs;

    @Min(value = 1, message ="min. attendees quantity must be positive number between 1 and 10")
    @Max(value = 10,message ="min. attendees quantity must be positive number between 1 and 10")
    private long minAtts;

    @NotEmpty(message = "course state must be filled by programm!")
    private String courseState;

    private String update;
    private String review;

    private String owner;

    public CourseModel() {
    }

    public void fillForNewCourse(){ //some time we need model with 'null' fields
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLinks() {
        return links;
    }

    public void setLinks(String links) {
        this.links = links;
    }

    public long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(long categoryId) {
        this.categoryId = categoryId;
    }

    public String getCourseState() {
        return courseState;
    }

    public void setCourseState(String courseState) {
        this.courseState = courseState;
    }

    public String getUpdate() {
        return update;
    }

    public void setUpdate(String update) {
        this.update = update;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public long getMinSubs() {
        return minSubs;
    }

    public void setMinSubs(long minSubs) {
        this.minSubs = minSubs;
    }

    public long getMinAtts() {
        return minAtts;
    }

    public void setMinAtts(long minAtts) {
        this.minAtts = minAtts;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }
}
