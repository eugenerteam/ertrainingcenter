package tc.server.source.models;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

/**
 * Created by evgeniy.ranchenko on 14.12.2016.
 */
public class EvaluationModel extends SubscribeModel{

    @Min(value = 1, message = "grade must be between 1 and 5")
    @Max(value = 5, message = "grade must be between 1 and 5")
    private long grade;

    public long getGrade() {
        return grade;
    }

    public void setGrade(long grade) {
        this.grade = grade;
    }
}
