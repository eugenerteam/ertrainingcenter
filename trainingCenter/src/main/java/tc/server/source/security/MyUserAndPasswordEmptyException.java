package tc.server.source.security;

import org.springframework.security.core.userdetails.UsernameNotFoundException;

/**
 * Created by evgeniy.ranchenko on 16.12.2016.
 */
public class MyUserAndPasswordEmptyException extends UsernameNotFoundException {
    public MyUserAndPasswordEmptyException(String msg) {
        super(msg);
    }

    public MyUserAndPasswordEmptyException(String msg, Throwable t) {
        super(msg, t);
    }
}
