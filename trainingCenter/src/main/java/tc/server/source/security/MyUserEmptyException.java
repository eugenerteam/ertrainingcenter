package tc.server.source.security;

import org.springframework.security.core.userdetails.UsernameNotFoundException;

/**
 * Created by evgeniy.ranchenko on 16.12.2016.
 */
public class MyUserEmptyException extends UsernameNotFoundException {
    public MyUserEmptyException(String msg) {
        super(msg);
    }

    public MyUserEmptyException(String msg, Throwable t) {
        super(msg, t);
    }
}
