package tc.server.source.security;

import org.springframework.security.core.userdetails.UsernameNotFoundException;

/**
 * Created by evgeniy.ranchenko on 16.12.2016.
 */
public class MyPasswordEmptyException extends UsernameNotFoundException {

    public MyPasswordEmptyException(String msg) {
        super(msg);
    }

    public MyPasswordEmptyException(String msg, Throwable t) {
        super(msg, t);
    }
}
