package tc.server.source.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

/**
 * Created by evgeniy.ranchenko on 16.12.2016.
 */
@Service("MyAuthProvider")
public class MyAuthProvider extends AbstractUserDetailsAuthenticationProvider {
    @Autowired
    MyUserDetailService myUserDetailService;

    public MyAuthProvider() {
        super();
        super.hideUserNotFoundExceptions = false;
    }

    @Override
   protected void additionalAuthenticationChecks(UserDetails userDetails, UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
        if (authentication.getCredentials() == null) {
            throw new BadCredentialsException(messages.getMessage(
                    "password is empty",
                    "Bad credentials"));
        }

        String presentedPassword = authentication.getCredentials().toString();

        if (!userDetails.getPassword().equals(presentedPassword))
            throw new BadCredentialsException(messages.getMessage(
                    "Invalid password",
                    "Bad credentials"));

    }

    @Override
    protected UserDetails retrieveUser(String s, UsernamePasswordAuthenticationToken token) throws AuthenticationException {
        boolean emptyUser = (s==null||s.trim().equals(""));
        boolean emptyPassword = (token.getCredentials()==null || token.getCredentials().toString().trim().equals(""));
        if (emptyUser&&emptyPassword) throw new MyUserAndPasswordEmptyException("aaa!");
        else if (emptyPassword) throw new MyPasswordEmptyException("ooo!");
        else if (emptyUser) throw new MyUserEmptyException("uuu!");

        UserDetails userDetails = myUserDetailService.loadUserByUsername(s);
        return userDetails;
    }
}
