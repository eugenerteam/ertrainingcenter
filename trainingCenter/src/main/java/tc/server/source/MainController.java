package tc.server.source;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by User on 26.11.2016.
 */
@Controller
@RequestMapping("/")
public class MainController {

    @RequestMapping("/{path}")
    public String redirect1(){
        System.out.println("m");
        return "redirect:/login";
    }

    @RequestMapping
    public String redirect(){
        System.out.println("m");
        return "redirect:/login";
    }
}
