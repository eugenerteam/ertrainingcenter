package tc.server.source.mail;


import tc.server.source.dao.EventsDao;
import tc.server.source.dao.SubscriptionDao;
import tc.server.source.dao.UserDao;
import tc.server.source.models.EmailModel;
import tc.server.source.tables.Event;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring4.SpringTemplateEngine;

import javax.mail.internet.MimeMessage;
import java.util.*;

/**
 * Created by evgeniy.ranchenko on 12.12.2016.
 */
@Service("mailService")
public class MailService {
    /*Email From*/
    public static final String FROM = "from";
    /*Email To*/
    public static final String TO = "to";
    /*Email Subject*/
    public static final String SUBJECT = "subject";
    /*Email Subject*/
    public static final String MESSAGE = "message";
    /*Email BCC*/
    public static final String BCC_LIST = "bccList";
    /*Email CCC*/
    public static final String CCC_LIST = "ccList";

    @Autowired
    JavaMailSender mailSender;

    @Autowired
    SpringTemplateEngine mailTemplateEngine;

    @Autowired
    Context context;

    @Autowired
    EventsDao eventsDao;

    @Autowired
    UserDao userDao;

    @Autowired
    SubscriptionDao subscriptionDao;

    public boolean sendEmail(final String templateName, final Map<String,Object> model){
        boolean result = false;
        try{
            MimeMessagePreparator preparator = new MimeMessagePreparator() {
                @Override
                public void prepare(MimeMessage mimeMessage) throws Exception {
                    String from = (String) model.get(FROM);
                    String[] to = (String[]) model.get(TO);
                    String[] copyTo = (String[]) model.get(CCC_LIST);
                    String subject = (String) model.get(SUBJECT);
                    String text = (String) model.get(MESSAGE);

                    List<String> bccList = (List<String>) model.get(BCC_LIST);
                    //ВАЖНО! ПОСТАВЬТЕ КОДИРОВКУ UTF-8 ИЛИ СООБЩЕНИЯ БУДУТ ?????? ??
                    MimeMessageHelper message = new MimeMessageHelper(mimeMessage, "UTF-8"); //ENCODING IMPORTANT!
                    message.setFrom(from);
                    message.setTo(to);
                    message.setCc(copyTo);
                    message.setSubject(subject);
                    message.setSentDate(new Date());
                    if (bccList != null) {
                        for (String bcc : bccList) {
                            message.addBcc(bcc);
                        }
                    }

                    model.put("noArgs", new Object());

                    context.setVariables(model);

                    text = mailTemplateEngine.process(templateName,context);
                    message.setText(text,true);

                    System.out.println(text);
                }
            };

            //mailSender.send(preparator);
            result = true;
        }catch (Exception ex){
            ex.printStackTrace();
        }

        return result;
    }

    public boolean PrepareAndSend(EmailModel emailModel){
        Event event = eventsDao.getEventByName((String) emailModel.get(emailModel.EVENT));

//        String[] to_arr = {"rewgen1@gmail.com"};
//        String[] cc_arr = {"rewgen@tut.by"};
        String[] to_arr = getEmailByMask(event.getRecipient(), (Long) emailModel.get(emailModel.COURSE_ID));
        String[] cc_arr = getEmailByMask(event.getCopyRecipient(), (Long) emailModel.get(emailModel.COURSE_ID));

        Map<String, Object> model1 = new HashMap<>();
        model1.put(MailService.FROM, "evgeniy.ranchenko@servolux.by");
        model1.put(MailService.SUBJECT, event.getSubject());
        model1.put(MailService.TO, to_arr);
        model1.put(MailService.CCC_LIST, cc_arr);
//        model1.put(MailService.BCC_LIST, new ArrayList<>());
        model1.put("userName", "evgeniy.ranchenko"); //robot-sender name

        for (Map.Entry<String,Object> entry : emailModel.getAsMap().entrySet())
            model1.put(entry.getKey(),entry.getValue());
        return sendEmail(event.getTemplate(), model1);
    }

    public String[] getEmailByMask(String mask, Long courseId){
        List<String> emails = new ArrayList<>();

        String[] elements = mask.split("\\,");
        if (elements!=null){
            for (String maskElement : elements){
                if (maskElement.contains("role:")){
                    emails.addAll(userDao.getUsenamesByRoleMask(maskElement));
                }else if (maskElement.contains("type:")){
                    emails.addAll(subscriptionDao.getUsenamesByUserTypeMask(mask, courseId));
                }
            }
        }
        return  emails.toArray(new String[emails.size()]);
    }

}
